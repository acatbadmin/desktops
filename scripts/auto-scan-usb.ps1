# References:
#   - https://superuser.com/a/845411/1738993

# Ensure the event is registered.
$eventSID = 'volumeChange'
$eventSubscribers = Get-EventSubscriber
if (!(Get-EventSubscriber | Where-Object {$_.SourceIdentifier -eq $eventSID})) {
    Register-CimIndicationEvent -Class win32_VolumeChangeEvent -SourceIdentifier $eventSID
}

while (1 -eq 1) {
    $newEvent = Wait-Event -SourceIdentifier $eventSID
    $eventType = $newEvent.SourceEventArgs.NewEvent.EventType
    # 1: cfg. change; 2: device added; 3: device removed; 4: docked
    Write-Host "VolumeChangeEvent captured."
    if ($eventType -eq 2) {
        Write-Host "USB device added; waiting 1 second..."
        $driveLetter = $newEvent.SourceEventArgs.NewEvent.DriveName
        Start-Sleep -Seconds 1
        # Could use array below...
        if ($driveLetter -eq 'E:' -or $driveLetter -eq 'F:') {
            $scanPath = $driveLetter + '\'
            Write-Host "Starting MpScan for $scanPath"
            Start-MpScan -ScanType CustomScan -ScanPath $scanPath
            Write-Host "Scan complete."
        }
    }
    # Remove event after handling.
    Remove-Event -SourceIdentifier $eventSID
}

# Ensure the event is unregistered.
Unregister-Event -SourceIdentifier $eventSID
