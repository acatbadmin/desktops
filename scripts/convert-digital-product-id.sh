#!/usr/bin/env bash

if [[ ! $(which chntpw) ]]; then
    echo "Error: Need to install chntpw"
    exit 1
fi

reg_file="$1"

fprint() {
    key=$1
    echo ${key:0:5}-${key:5:5}-${key:10:5}-${key:15:5}-${key:20:5}
}

# Get full registry key output from chntpw.
reg_value_chntpw=$(echo -e "hex \\Microsoft\\Windows NT\\CurrentVersion\\DigitalProductId\nq\n" \
    | chntpw -e "${reg_file}" | grep "^:" | cut -b 9-55)
# Convert to hex values separated by commas.
hex_productid_full=$(echo $reg_value_chntpw | tr ' ' ',' | tr '\n' ',' | tr '[:upper:]' '[:lower:]')
# Keep only values relevant for DigitalProductId.
hex_productid=$(echo $hex_productid_full | cut -d, -f53-68)

# Almost a direct conversion of c# code from:
#   https://github.com/mrpeardotnet/WinProdKeyFinder/blob/master/WinProdKeyFind/KeyDecoder.cs
# However most of this looks similar to sudo code I found:
#   /ubuntu/953126/can-i-recover-my-windows-product-key-from-ubuntu
digits=(B C D F G H J K M P Q R T V W X Y 2 3 4 6 7 8 9)
for j in {0..15}; do
    # Populate the Pid array from the values found in the registry
    Pid[$j]=$((16#$(echo ${hex_productid} |cut -d, -f $(($j+1)))))
done

# modifications needed for getting the windows 8+ key
isWin8=$(( $(( ${Pid[14]}/6 )) & 1 ))
Pid[14]=$(( $(( ${Pid[14]} & 247 )) | $(( $(( ${isWin8} & 2 )) * 4 )) ))
key=""
last=0
for i in {24..0};do
    current=0
    for j in {14..0};do
        # Shift the current contents of c to the left by 1 byte
        # and add it with the next byte of our id
        current=$((${current}*256))
        current=$((${Pid[$j]} + current))
        # Put the result of the divison back into the array
        Pid[$j]=$((${current}/24))
        # Calculate remainder of c
        current=$((${current}%24))
        last=${current}
    done
    # Take character at position c and prepend it to the ProductKey
    key="${digits[${current}]}${key}"
done

fprint "${key}"
