﻿# ========================================================================
# This script is for configuring Windows 10 machines at ACATBA
# ========================================================================
# Bypass ExecutionPolicy by running this script with:
# \> powershell.exe -ExecutionPolicy bypass .\configuration-user.ps1


function Unpin-App([string]$appname) {
    $app = (New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() |
        ?{$_.Name -eq $appname}
    If ($app) {
        Write-Host "Désépinglage de $app.Name"
        $app.Verbs() |
            ?{$_.Name.replace('&','') -match "Désépingler.*"} |
            %{$_.DoIt()}
    }
}


$Script = $MyInvocation.MyCommand.Path
$ScriptsDir = Split-Path $Script -Parent
$Repo = Split-Path $ScriptsDir -Parent
$RepoParent = Split-Path "$Repo" -Parent
$AppsDir = "$RepoParent\apps"

# User locale stuff.
Set-Culture fr-CF
Set-WinUserLanguageList fr-FR -Force # also sets KB to AZERTY
$layout = 'azerty'
$answer = Read-Host "Clavier configuré comme AZERTY. Modifier-le en QWERTY ? [o/N] "
If ($answer -eq 'o') {
    $layout = 'qwerty'
    # Get current LanguageList & modify it.
    $l = Get-WinUserLanguageList
    $l[0].InputMethodTips.Add("040C:00020409") # US-Int'l.
    $l[0].InputMethodTips.Remove("040C:0000040C") # FR-AZERTY
    # Set new list.
    Set-WinUserLanguageList -LanguageList $l -Force
}

# Install Multilingual keyboard.
$answer = Read-Host "Installer le clavier multilangue ($layout) ? [o/N] "
If ($answer -eq 'o') {
    $dir = 'unzipped'

    # Define installer file path.
    If ($layout -eq 'qwerty') {
        $zip = "$AppsDir\Cameroon QWERTY Unified*.zip"
        $ipath = "$dir\Cameroon_QWERTY_Unified_2017\CAMQ2017_amd64.msi"
        $mpath = "$dir\Cameroon_QWERTY_Unified_2017\Manuel clavier.pdf"
    } Else {
        $zip = "$AppsDir\Cameroon AZERTY*.zip"
        $ipath = "$dir\cama2021\CAMA2021_amd64.msi"
        $mpath = "$dir\Readme Azerty Francais.pdf"
    }

    # Unzip archive.
    Expand-Archive -Path "$zip" -DestinationPath "$dir"

    # Run installer.
    $ArgString = "/passive"
    $inst = Start-Process -FilePath "$ipath" -ArgumentList "$ArgString"
    
    # Copy the manual to the Desktop
    Copy-Item -Path "$mpath" -Destination "$HOME\Desktop\Manuel clavier.pdf"

    # Delete temp dir.
    Remove-Item -LiteralPath "$dir" -Force -Recurse
}

# Unpin unnecessary apps.
# Unpin-App("Microsoft Edge")  # new default
# Unpin-App("Microsoft Store")  # UnauthorizedAccessException
Unpin-App("Cortana")
Unpin-App("Courriel")
Unpin-App("Mail")

# Launch backup config window.
sdclt /configure

Write-Host "Configuration d'utilisateur terminée."
