﻿# This will self elevate the script with a UAC prompt since this script needs to be run as an Administrator in order to function properly.
$IsAdmin = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]'Administrator')
If (!($IsAdmin)) {
    Write-Host "Relancement en tant qu'Administrator dans 3 secondes."
    Start-Sleep 3
    Start-Process powershell.exe -ArgumentList ("-NoProfile -ExecutionPolicy Bypass -File `"{0}`"" -f $PSCommandPath) -Verb RunAs
    Exit
}

# Set directory paths.
$Script = $MyInvocation.MyCommand.Path
$ScriptsDir = Split-Path "$Script" -Parent
$Repo = Split-Path "$ScriptsDir" -Parent
$RepoParent = Split-Path "$Repo" -Parent
$AppsDir = "$RepoParent\apps"
$FontsDir = "$RepoParent\polices"
$ConfigDir = "$Repo\config"


Function Get-Installed($1) {
    If (!($1)) {
        $name = '*'
    } Else {$name = $1}
    Get-ItemProperty -path HKLM:\software\Wow6432Node\microsoft\windows\currentversion\uninstall\*, `
        HKLM:\software\microsoft\windows\currentversion\uninstall\* |
        Select-Object DisplayName,DisplayVersion | Where-Object {$_.DisplayName -Like "*$name*"}
}

function Install-Font {
    param([string] $zip)
    $dir = 'unzipped'
    Expand-Archive -Path "$zip" -DestinationPath "$dir"

    foreach ($f in Get-ChildItem -Path $dir -File) {
        $dest = "C:\Windows\Fonts\$f"
        if (Test-Path -Path $dest) {
            "Police $f déjà instalée."
        }
        else {
            "$f" | Copy-Item -Destination "$dest"
        }
    }
    Remove-Item -LiteralPath "$dir" -Force -Recurse
}


# Install fonts.
Write-Host "Installation des polices..."
foreach ($font in Get-ChildItem -Path "$fontsDir" | ForEach-Object {$_.FullName}) {
    Install-Font "$font" -ErrorAction SilentlyContinue
}

# # Install Firefox.
# $AppName = "Firefox"
# $answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
# If ($answer -eq 'o') {
#     $FilePath = Get-ChildItem "$AppsDir\Firefox Setup *.exe"
#     $ArgString = "/SILENT"
#     Start-Process -FilePath "$FilePath" -ArgumentList "$ArgString"
# }

# # Install Chrome.
# $AppName = "Chrome"
# $answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
# If ($answer -eq 'o') {
#     $FilePath = Get-ChildItem "$AppsDir\ChromeStandaloneSetup*.exe"
#     # $ArgString = "/SILENT" # arguments seem to not be permitted
#     Start-Process -FilePath "$FilePath"
# }

# # Install Thunderbird.
# $AppName = "Thunderbird"
# $answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
# If ($answer -eq 'o') {
#     $FilePath = Get-ChildItem "$AppsDir\Thunderbird Setup *.exe"
#     $ArgString = "/SILENT"
#     Start-Process -FilePath "$FilePath" -ArgumentList "$ArgString" -Wait
#     # Copy config file.
#     $CfgDir = "${env:ProgramFiles}\Mozilla Thunderbird\defaults\pref"
#     Copy-Item "$ConfigDir\thunderbird\SIL-CAR-cfg.js" -Destination "$CfgDir"
# }

# Install LibreOffice.
$AppName = "LibreOffice"
$answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
If ($answer -eq 'o') {
    Get-ChildItem "$AppsDir\LibreOffice_*.msi" | ForEach-Object {
        $ArgString = "UI_LANGS=fr,en_US /passive"
        $AppPath = $_.Name
        $FilePath = "$AppsDir\$AppPath"
        Start-Process -FilePath "$FilePath" -ArgumentList "$ArgString" -Wait
    }

    # Install LibreOffice config extension.
    # TODO: This will throw an error if already installed, so update might fail (depends on version?).
    $oxt = "$ConfigDir\libreoffice\acatba-config-addon.oxt"
    $ArgString = "add --shared --suppress-license $oxt"
    Start-Process -FilePath "$env:ProgramFiles\LibreOffice\program\unopkg.exe" -ArgumentList "$ArgString" -Wait
}

# Install Paratext.
$AppName = "Paratext"
$answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
If ($answer -eq 'o') {
    # Close Paratext if already running.
    If (Get-Process -ProcessName 'Paratext' -ErrorAction SilentlyContinue) {
        Stop-Process -ProcessName 'Paratext' -Confirm
    }
    $InstPath = Get-ChildItem "$AppsDir\Paratext_*InstallerOffline.exe"
    $PatchPath = Get-ChildItem "$AppsDir\patch_*msp"
    $ArgString = "/passive /norestart"

    $status = Get-Installed 'paratext'
    If (!($status.DisplayName)) {
        # Install Paratext base.
        Start-Process -FilePath "$InstPath" -ArgumentList "$ArgString" -Wait
        # Install Paratext patch, if available.
        If (Test-Path "$PatchPath") {
            Start-Process -FilePath "$PatchPath" -ArgumentList "$ArgString" -Wait
        }
        Write-Host "Paratext a été installé. Il faut ajouter les ressources manuellement."
    }
}

# Install VLC.
$AppName = "VLC"
$answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
If ($answer -eq 'o') {
    $FilePath = Get-ChildItem "$AppsDir\vlc-*.exe"
    $ArgString = "/SILENT"
    Start-Process -FilePath "$FilePath" -ArgumentList "$ArgString"
}

# Install ESET.
$AppName = "ESET"
$answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
$eset = $answer
If ($answer -eq 'o') {
    $FilePath = Get-ChildItem "$AppsDir\eis_nt64.exe"
    # $ArgString = "/silent /LANG=french /nostart"
    $inst = Start-Process -FilePath "$FilePath" -PassThru
    return "$inst"
}

# Install Avast if ESET not installed.
If ($eset -ne 'o') {
    $AppName = "Avast"
    $answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
    If ($answer -eq 'o') {
        $FilePath = Get-ChildItem "$AppsDir\avast_free_antivirus_setup_offline*.exe"
        $inst = Start-Process -FilePath "$FilePath"
    }
}

# Install Zoom.
$AppName = "Zoom"
$answer = Read-Host "Installer/mettre à jour ${AppName}? [o/N]"
If ($answer -eq 'o') {
    $FilePath = Get-ChildItem "$AppsDir\ZoomInstallerFull.msi"
    $ArgString = "/passive /norestart"
    $inst = Start-Process -FilePath "$FilePath" -ArgumentList "$ArgString" -PassThru
    return "$inst"
}

# Reset working directory.
Set-Location "$StartDir"

Write-Host "Installation des applications terminée. Veuillez redémarrer la machine."
exit 0
