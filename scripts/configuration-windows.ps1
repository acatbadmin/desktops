﻿# # Continue silently on errors?
#  $PSDefaultParameterValues = $PSDefaultParameterValues.clone()
#  # Set ErrorAction for all CmdLets.
#  $PSDefaultParameterValues += @{'*:ErrorAction' = 'SilentlyContinue'}

function New-LocalAdmin {
    [CmdletBinding()]
    param (
        [string] $NewLocalAdmin,
        [securestring] $Password
    )
    process {
        If (Get-LocalGroup | Where-Object{$_.Name -eq 'Administrateurs'}) {
            $AdminGroup = 'Administrateurs'
        } Else {
            $AdminGroup = 'Administrators'
        }

        If (!(Get-LocalUser -Name "$NewLocalAdmin")) {
            New-LocalUser "$NewLocalAdmin" -Password $Password -FullName "Administrateur" -Description "Compte Administrateur" -AccountNeverExpires -PasswordNeverExpires
            Write-Host "Compte `"$NewLocalAdmin`" a été créé."
        } Else {
            Write-Host "Compte `"$NewLocalAdmin`" existe déjà."
        }
        If (!(Get-LocalGroupMember -Group "$AdminGroup" -Member "$NewLocalAdmin")) {
            Add-LocalGroupMember -Group "$AdminGroup" -Member "$NewLocalAdmin"
            Write-Host "Compte $NewLocalAdmin a été configuré comme Administrateur."
        } Else {
            Write-Host "Compte `"$NewLocalAdmin`" déjà configuré comme Administrateur."
        }
    }
}

function Set-RegPathItem {
    param (
        $path,
        $item,
        $type,
        $value
    )
    Write-Host "$path -> $item [$type] = $value"
    reg add "$path" /f /v "$item" /t "$type" /d "$value" | Out-Null
}


# This will self elevate the script with a UAC prompt since this script needs to be run as an Administrator in order to function properly.
$IsAdmin = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]'Administrator')
If (!($IsAdmin)) {
    Write-Host "Relancement en tant qu'Administrator dans 3 secondes."
    Start-Sleep 3
    Start-Process powershell.exe -ArgumentList ("-NoProfile -ExecutionPolicy Bypass -File `"{0}`"" -f $PSCommandPath) -Verb RunAs
    Exit
}

# Get HW information.
$RevDate = Get-Date
$SysInfo = Get-WmiObject -Class Win32_ComputerSystem
$BiosInfo = Get-WmiObject -Class Win32_BIOS
$Manufacturer = $SysInfo.Manufacturer
$ModelNumber = $SysInfo.Model
$SerialNmumber = $BiosInfo.SerialNumber
$MacAddr = Get-NetAdapter | Select-Object Name,MacAddress

# Get OS info.
$OSInfo = Get-ItemProperty -Path 'HKLM:\software\microsoft\windows nt\Currentversion'
[double]$OSVersion = $OSInfo.CurrentMajorVersionNumber
$Edition = $OSInfo.ProductName
$Release = $OSInfo.ReleaseId
$Lang = (Get-Culture).DisplayName

# Print info.
Write-Host "Détails du système:"
Write-Host "Date:`t`t$RevDate"
Write-Host "Make:`t`t$Manufacturer"
Write-Host "Model:`t`t$ModelNumber"
Write-Host "S/N:`t`t$SerialNumber"
Write-Host "MAC:`t`t$MacAddr"
Write-Host "OS:`t`t$OSVersion"
Write-Host "Edition:`t$Edition"
Write-Host "Release:`t$Release"
Write-Host "Language:`t$Lang"
Write-Host "UI Lang.:`t$PSUICulture"


# Ensure Admin account.
$NewLocalAdmin = "Admin"
# $Password = Read-Host -AsSecureString "Mot de passe pour nouveau compte `"$NewLocalAdmin`""
$Password = ConvertTo-SecureString -String "administrator" -AsPlainText -Force
# New-LocalAdmin -NewLocalAdmin "$NewLocalAdmin" -Password "$Password"
# Set correct Admin group  name.
If (Get-LocalGroup | Where-Object{$_.Name -eq 'Administrateurs'}) {
    # $UserGroup = 'Utilisateurs'
    $AdminGroup = 'Administrateurs'
} Else {
    # $UserGroup = 'Users'
    $AdminGroup = 'Administrators'
}
# Ensure Admin user.
If (!(Get-LocalUser -Name "$NewLocalAdmin")) {
    # NOTE: Can't figure out how to make New-LocalUser accessible at login.
    # New-LocalUser "$NewLocalAdmin" -Password $Password -FullName "Administrateur" -Description "Compte Administrateur" -AccountNeverExpires -PasswordNeverExpires
    # NOTE: "NET USER" command requires plain text password.
    $Password = "administrator"
    NET USER "$NewLocalAdmin" "$Password" /ADD
    # Add-LocalGroupMember -Group "$UserGroup" -Member "$NewLocalAdmin"
    Write-Host "Compte `"$NewLocalAdmin`" a été créé."
} Else {
    Write-Host "Compte `"$NewLocalAdmin`" existe déjà."
}
# Ensure that Admin user is added to Admin group.
If (!(Get-LocalGroupMember -Group "$AdminGroup" -Member "$NewLocalAdmin")) {
    Add-LocalGroupMember -Group "$AdminGroup" -Member "$NewLocalAdmin"
    Write-Host "Compte $NewLocalAdmin a été configuré comme Administrateur."
} Else {
    Write-Host "Compte `"$NewLocalAdmin`" déjà configuré comme Administrateur."
}

# Set system locale stuff.
Set-WinSystemLocale -SystemLocale fr-FR
Set-WinHomeLocation -GeoId 55 # CAR

# Fix timezone setting for WAT.
$tzobj = Get-TimeZone
$tzid = 'W. Central Africa Standard Time'
if ( "$tzobj.Id" -ne "$tzid" ) {
    Write-Host "Configuration de fuseau horaire à WAT..."
    Set-TimeZone -Id "$tzid"
} else {
    Write-Host "Fuseau horaire est déjà configuré pour WAT"
}
# Ensure time is set to auto-sync.
Write-Host "Configuration d'auto-synchronisation de l'heure..."
net stop w32time
w32tm /unregister
w32tm /register
net start w32time
w32tm /resync /nowait

# Make registry changes.
Write-Host
Write-Host "Commencement de la modification du régistre."
# Choose when updates are installed (Windows Updates - Advanced Options)
#   Semi-Annual = 0x20 : ready for widespread use in organizations
#   Semi-Annual (Targeted) = 0x10 : ready for use by most gullible consumers
$path = "HKEY_LOCAL_MACHINE\Software\Microsoft\WindowsUpdate\UX\Settings"
Set-RegPathItem "$path" "BranchReadinessLevel" "REG_DWORD" "32"

# Choose if the store apps can auto-update. (Windows Store - Settings).
#   2  = disabled, 4 = auto download
$path = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsStore\WindowsUpdate"
Set-RegPathItem "$path" "AutoDownload" "REG_DWORD" "2"

# gpedit.msc Computer - Admin Templates - Windows Components - Delivery Optimization
#  Download mode: 0 = http only - disable peering
#                 1 = LAN only
#                 2 = Active Directory Group
#                 3 = Internet
#                 99 = disabled.
#                 100 = disabled. Use BITS instead. Not recommended
# Debugging
# peers = Get-DeliveryOptimizationPerfSnap | Select NumberOfPeers
# port = 7680. Need to be able to access that (via telnet for example)
# https://docs.microsoft.com/en-us/windows/deployment/update/waas-delivery-optimization-reference#download-mode
# New-PSDrive -Name HKU -PSProvider Registry -Root HKEY_USERS > $null
$path = "HKEY_USERS\S-1-5-20\Software\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config"
Set-RegPathItem "$path" "DODownloadMode" "REG_DWORD" "1"

# Network - Data Usage: Restrict background data to help reduce data Usage.
#    Limit what store apps and Windows features can do in the background
$path = "HKEY_CURRENT_USER\SOFTWARE\Microsoft\DusmSvc\Profiles\{A4E15004-C564-45CA-98F6-F32E14BC8EAA}"
Set-RegPathItem "$path" "BackgroundRestriction" "REG_DWORD" "4"

# # Privacy - Background Apps - Let Apps Run in background
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications"
# Set-RegPathItem "$path" "GlobalUserDisabled" "REG_DWORD" "1"
# Set-RegPathItem "$path" "Migrated" "REG_DWORD" "4"

# Apps and Features : Choose where you can get apps.
#   Turn off app recommendations = "Anywhere"
#   Show me app recommendations = "Recommendations"
#   Warn me before installing apps from outside of the store = "PreferStore"
#   Allow apps from the store only = "StoreOnly"
$path = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer"
Set-RegPathItem "$path" "AicEnabled" "REG_SZ" "Anywhere"

# # Set Windows Defender to scan USB drives during Full scans. (WARNING: no notifications during scan unless virus found)
$path = "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows Defender\Scan"
Set-RegPathItem "$path" "DisableRemovableDriveScanning" "REG_DWORD" "0"

# Apps : Video playback : I prefer video to play at a lower resolution (to help save network bandwidth)
$path = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\VideoSettings"
Set-RegPathItem "$path" "AllowLowResolution" "REG_DWORD" "1"

# # Let apps use advertising ID to make ads more interesting to you based on your app activity.
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo"
# Set-RegPathItem "$path" "Enabled" "REG_DWORD" "0"
# reg delete "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo" /f /v Id 2>&1 | Out-Null

# # Online speech recognition. (Privacy - Speech)
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Speech_OneCore\Settings\OnlineSpeechPrivacy"
# Set-RegPathItem "$path" "HasAccepted" "REG_DWORD" "0"

# # Tailored experiences: Let MS offer you tailored experiences (Privacy - Diagnostics & feedback)
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Speech_OneCore\Settings\OnlineSpeechPrivacy"
# Set-RegPathItem "$path" "TailoredExperiencesWithDiagnosticDataEnabled" "REG_DWORD" "0"

# Basic (1) or Full(3) diagnostic data to send to MS (Privacy - Diagnostics & feedback)
# TODO: Can this value be set to "0"?
$path = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Diagnostics\DiagTrack"
Set-RegPathItem "$path" "ShowedToastAtLevel" "REG_DWORD" "0"
$path = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\DataCollection"
Set-RegPathItem "$path" "AllowTelemetry" "REG_DWORD" "1"

# # Feedback frequency: Windows should ask for my feedback...
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Siuf\Rules"
# Set-RegPathItem "$path" "NumberOfSIUFInPeriod" "REG_DWORD" "0"

# # Ink & typing personalization: Use your typing patterns etc - 5 settings
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Personalization\Settings"
# Set-RegPathItem "$path" "AcceptedPrivacyPolicy" "REG_DWORD" "0"
# $path = "HKEY_CURRENT_USER\Software\Microsoft\InputPersonalization"
# Set-RegPathItem "$path" "RestrictImplicitInkCollection" "REG_DWORD" "1"
# $path = "HKEY_CURRENT_USER\Software\Microsoft\InputPersonalization"
# Set-RegPathItem "$path" "RestrictImplicitTextCollection" "REG_DWORD" "1"
# $path = "HKEY_CURRENT_USER\Software\Microsoft\InputPersonalization\TrainedDataStore"
# Set-RegPathItem "$path" "HarvestContacts" "REG_DWORD" "0"
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\SettingSync\Groups\Language"
# Set-RegPathItem "$path" "Enabled" "REG_DWORD" "0"

# # Windows Ink Workspaces: Show recommended app suggestions (Devices - Pen & Windows Ink)
# $path = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\PenWorkspace"
# Set-RegPathItem "$path" "PenWorkspaceAppSuggestionsEnabled" "REG_DWORD" "0"

# # Turn off Start Menu and Taskbar transparency effects.
# # https://www.maketecheasier.com/increase-transparency-of-start-menu-windows10/
# $path = "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize"
# Set-RegPathItem "$path" "EnableTransparency" "REG_DWORD" "0"
Write-Host # end of registry changes

# Disable BitLocker disk encryption.
Write-Host "Désactivation de cryptage de BitLocker..."
Get-BitLockerVolume | ForEach-Object -Process {
    if (($_.VolumeStatus -ne 'FullyDecrypted') -or ($_.ProtectionStatus -ne 'Off')) {
        Disable-BitLocker -MountPoint $_
        Write-Host "BitLocker a été désactivé sur '$_'"
    }
}
