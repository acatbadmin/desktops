# ========================================================================
# This script is for configuring Windows 10 machines at ACATBA
# ========================================================================

# Run script with ExecutionPolicy bypassed  temporarily with:
# \> powershell.exe -ExecutionPolicy Bypass -File "{0}"

$PSDefaultParameterValues = $PSDefaultParameterValues.clone()
# Set ErrorAction for all CmdLets.
$PSDefaultParameterValues += @{'*:ErrorAction' = 'SilentlyContinue'}

# This will self elevate the script with a UAC prompt since this script needs to be run as an Administrator in order to function properly.
$IsAdmin = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]'Administrator')
If (!($IsAdmin)) {
    Write-Host "Relancement en tant qu'Administrator dans 3 secondes."
    Start-Sleep 3
    Start-Process powershell.exe -ArgumentList ("-NoProfile -ExecutionPolicy Bypass -File `"{0}`"" -f $PSCommandPath) -Verb RunAs
    Exit
}

# Set folder paths.
$Script = $MyInvocation.MyCommand.Path
$Repo = Split-Path $Script -Parent
$ScriptsDir = "$Repo\scripts"

# Run system config script.
& "$ScriptsDir\configuration-system.ps1"

# Run user config script.
& "$ScriptsDir\configuration-user.ps1"

# Install apps.
& "$ScriptsDir\installation-applis.ps1"
